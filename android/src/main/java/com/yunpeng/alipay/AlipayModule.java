package com.yunpeng.alipay;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.alipay.sdk.app.AuthTask;
import com.alipay.sdk.app.OpenAuthTask;
import com.alipay.sdk.app.PayTask;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by m2mbob on 16/5/6.
 */
public class AlipayModule extends ReactContextBaseJavaModule{

    private static final int SDK_PAY_FLAG = 1;
    private static final String TAG = "AlipayModule";

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler(getReactApplicationContext().getMainLooper()) {
        @SuppressWarnings("unused")
        public void handleMessage(Message msg) {
            try{
                switch (msg.what) {
                    case SDK_PAY_FLAG: {
                        String resultStatus = (String) msg.obj;
                        // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                        if (TextUtils.equals(resultStatus, "9000")) {
                            Toast.makeText(getCurrentActivity(), "支付成功", Toast.LENGTH_SHORT).show();
                        } else {
                            // 判断resultStatus 为非"9000"则代表可能支付失败
                            // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                            if (TextUtils.equals(resultStatus, "8000")) {
                                Toast.makeText(getCurrentActivity(), "支付结果确认中", Toast.LENGTH_SHORT).show();

                            } else {
                                // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                                Toast.makeText(getCurrentActivity(), "支付失败", Toast.LENGTH_SHORT).show();

                            }
                        }
                        break;
                    }
                    default:
                        break;
                }
            }catch (Exception e){
                Log.d(TAG, "error: " + e.toString());
            }
        };
    };

    public AlipayModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @ReactMethod
    public void pay(final String payInfo,
                    final Promise promise) {

        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    PayTask alipay = new PayTask(getCurrentActivity());
                    String result = alipay.pay(payInfo, true);
                    PayResult payResult = new PayResult(result);
                    String resultInfo = payResult.getMemo();
                    String resultStatus = payResult.getResultStatus();
                    Message msg = new Message();
                    msg.what = SDK_PAY_FLAG;
                    msg.obj = resultStatus;
                    mHandler.sendMessage(msg);
                    if(Integer.valueOf(resultStatus) >= 8000){
                        promise.resolve(result);
                    }else{
                        promise.reject(resultInfo, new RuntimeException(resultStatus+":"+resultInfo));
                    }
                } catch (Exception e) {
                    promise.reject(e.getLocalizedMessage(), e);
                }
            }
        };

        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    @ReactMethod
    public void oauth(final String infoStr, final Promise promise) {
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                AuthTask authTask = new AuthTask(getCurrentActivity());
//                Map<String, String> map = authTask.authV2(infoStr, true);
//                promise.resolve(getWritableMap(map));
//            }
//        };
//        Thread thread = new Thread(runnable);
//        thread.start();
        // 传递给支付宝应用的业务参数
        final Map<String, String> bizParams = new HashMap<>();
        bizParams.put("url", infoStr);
        final String scheme = "ctmail";
        // 唤起授权业务
        final OpenAuthTask task = new OpenAuthTask(getCurrentActivity());
        task.execute(
                scheme, // Intent Scheme
                OpenAuthTask.BizType.AccountAuth, // 业务类型
                bizParams, // 业务参数
                new OpenAuthTask.Callback() {
                    @Override
                    public void onResult(int i, String s, Bundle bundle) {
                    Log.i("-----",String.format("业务成功，结果码: %s\n结果信息: %s\n结果数据: %s", i, s, bundleToString(bundle)));
                   promise.resolve(getWritableMapByBundle(bundle));
                    }
                }, // 业务结果回调。注意：此回调必须被你的应用保持强引用
                true); // 是否需要在用户未安装支付宝 App 时，使用 H5 中间页中转。建议设置为 true。
    }
    private WritableMap getWritableMap(Map<String, String> map) {
        WritableMap writableMap = Arguments.createMap();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            writableMap.putString(entry.getKey(), entry.getValue());
        }
        return writableMap;
    }
    private WritableMap getWritableMapByBundle(Bundle bundle) {
        WritableMap writableMap = Arguments.createMap();
        for (String key: bundle.keySet()) {
            writableMap.putString(key, (String) bundle.get(key));
        }
        return writableMap;
    }
    private static String bundleToString(Bundle bundle) {
        if (bundle == null) {
            return "null";
        }
        final StringBuilder sb = new StringBuilder();
        for (String key: bundle.keySet()) {
            sb.append(key).append("=>").append(bundle.get(key)).append("\n");
        }
        return sb.toString();
    }
    @Override
    public String getName() {
        return "AlipayModule";
    }

}
